class FilterParams
  attr_reader :data
  attr_reader :errors

  def initialize(params, check_defore_save=false)
    @success = false
    @errors = []
    @params = params
    @check_defore_save = check_defore_save

    get_params
  end

  def get_params
    params = filter_params

    if @errors.empty?
      @data = params
      @success = true
    end
  end

  def success?
    @success
  end

  def max_distance_format params
    if params['max_distance'].present?
      if params['max_distance'].to_i == 0
        @errors << 'Set max distance grater than 0 or ignore this option'
        return false
      end
    else
      params['max_distance'] = nil
    end

    params
  end

  def filter_params
   params = @params
    # при использовании сервиса для сохранения фильтра - проверяем, что есть хоть какие-то полезные параметры
    if @check_defore_save
      unless check_params_existance params
        return false
      end
    end

    # валидация на наличие обоих границ в часах если хоть одна задана
    if params['start_from'].present? || params['start_to'].present?
      unless [params['start_from'].present?, params['start_to'].present?].all?
        @errors << 'You should set start_from and start_to together or ignore filter by hours'
        return false
      end
    end

    # обрабатываем даты, получаем в нужном формате все параметры, пустые приводим к nil
    params = ParseDateParams.new(params)

    if params.success?
      params = max_distance_format params.data
      params
    else
      @errors += params.errors
    end
  end

  def check_params_existance params
    res = [:start_from, :start_to, :start_date, :max_distance].map { |attr| params[attr.to_s].present? ? 1 : nil }
    if !params['game_ids'].present?
      res << nil
    else
      res << (params['game_ids'].reject(&:empty?).any? ? 1 : nil)
    end

    if res.all?(&:nil?)
      @errors << "Please set search conditions."
      false
    else
      true
    end
  end

end