class FiltersController < ApplicationController
  before_action :set_filter, only: [:show, :edit, :update, :destroy]

  # для наглядности - проверить что фильтры создаются, удалтиь ненужные
  def index
    @filters = current_user.filters
  end

  def destroy
    notifications = Notification.where(user: current_user, filter: @filter)
    notifications.destroy_all if notifications.any?
    @filter.destroy

    respond_to do |format|
      format.html { redirect_to filters_url, notice: 'Filter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_filter
      @filter = Filter.find(params[:id])
    end

end
