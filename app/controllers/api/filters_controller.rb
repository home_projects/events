class Api::FiltersController < Api::BaseController

  def create
      prepared_params = FilterParams.new(filter_params, true)
      if prepared_params.success?
        data = prepared_params.data

        data.delete_if { |key, value| !key.to_s.in? ['start_from', 'start_to', 'start_date', 'max_distance', 'game_ids'] }

        @filter = current_user.filters.new(data)

        if @filter.save
          render json: {success: true, filter: @filter.to_json}
        else
          render json: {errors: @filter.errors.to_json}, status: :unprocessable_entity
        end

      else
        render json: {errors: prepared_params.errors.to_json}, status: :unprocessable_entity
      end
  end

  private

    def filter_params
      params.fetch(:filter, {}).permit(:start_from, :start_to, :start_date, :max_distance, game_ids: [])
    end

end
