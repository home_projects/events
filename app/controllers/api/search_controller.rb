class Api::SearchController < Api::BaseController

  def events
    events = FindEvents.new(filter_params, current_user)

    if events.success?
      render json: {data: events.data.to_json}
    else
      render json: {errors: events.errors.to_json}, status: 422
    end

  end

  private

    def filter_params
      params.fetch(:filter, {}).permit(:start_from, :start_to, :start_date, :max_distance, game_ids: [])
    end

end
