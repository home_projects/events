class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show]

  # для наглядности
  def index
    @notifications = current_user.notifications.where(reviewed: false).includes([:user, :event])
  end

  def show
    @notification.update(reviewed: true)
  end

  private
    def set_notification
      @notification = Notification.find(params[:id])
    end

end
