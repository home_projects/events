class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    user_posts_path(resource.id)
  end

  def current_user
    (Rails.env.development? || Rails.env.test?) ? User.first : super
  end

end
