$ ->
  App.filters =

    find: (button) ->
      $.ajax
        url: button.data('action')
        type: 'GET'
        dataType: "json"
        data: $('#new_filter').serialize()
        success: (data) ->
          if data.hasOwnProperty('data')
            data = JSON.parse(data.data)
            template = $('.event-template').first()
            table = $('#events_table')
            container = table.find('tbody')
            table.find('.event-template').not('.none').remove()
            if data.length == 0
              App.flashes.showMsg('info', "Can't find events by this conditions")
            $.each(data, (index, event) ->
              event_tr = template.clone()
              event_tr.find('.name').text(event.name)
              event_tr.find('.start_at').text(moment(event.start_at, moment.ISO_8601).format('dddd DD.MM.YYYY HH:MM'))
              event_tr.find('.end_at').text(moment(event.end_at, moment.ISO_8601).format('dddd DD.MM.YYYY HH:MM'))
              event_tr.removeClass('none')
              container.append(event_tr)
            )

        error: (data) ->
          $.each(JSON.parse(data.responseJSON.errors), (index, error)->
            App.flashes.showMsg('error', error)
          )

    saveCallback: ->
      $('#new_filter').on 'ajax:success', ->
        App.flashes.showMsg('info', 'Filter has saved successfully.')
      $('#new_filter').on 'ajax:error', (e, data, status, xhr) ->
        $.each(JSON.parse(data.responseJSON.errors), (index, error)->
          App.flashes.showMsg('error', error)
        )
    datepickersInit: ->
      $('.hourpicker').datetimepicker({format: "HH:00 Z", keepOpen: false, showClose: true})
      $('.datepicker').datetimepicker({format: "dd Z", keepOpen: false, showClose: true})
      $('.datetimepicker').datetimepicker({format: "DD.MM.YYYY HH:MM Z", keepOpen: false, showClose: true})
      #widgetParent

    bindEvents: ->
      @datepickersInit()
      @saveCallback()

    init: ->
      @bindEvents()

  App.filters.init()