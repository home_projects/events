class Event < ActiveRecord::Base
  include Notifications

  reverse_geocoded_by :latitude, :longitude

  has_many :notifications, dependent: :destroy
  belongs_to :user
  has_many :events_games
  has_many :games, through: :events_games

  after_create :set_notifications
end
