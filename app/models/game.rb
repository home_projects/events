class Game < ActiveRecord::Base
  has_many :events_games
  has_many :events, through: :events_games
end
