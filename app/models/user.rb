class User < ActiveRecord::Base
  reverse_geocoded_by :latitude, :longitude

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :filters, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :events, dependent: :destroy

  # предполагаем что в таблице Users имеется его актуальный ip
  # подставляем для тестировки ip spb если он nil
  def ip
    # default spb '95.140.92.111'
    (Rails.env.development? || Rails.env.test?) ? (self.current_sign_in_ip ? self.current_sign_in_ip : '95.140.92.111') :  self.current_sign_in_ip
  end

  def coordinates
    # free: < 10 per/second
    Geocoder.coordinates(ip)
  end

  def latitude
    coordinates[0]
  end

  def longitude
    coordinates[1]
  end

  def active_notifications
    notifications.where(reviewed: false)
  end

end
