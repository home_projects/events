module Notifications
  extend ActiveSupport::Concern

  def set_notifications
    event = self

    users = User.all.where.not(id: self.user_id).includes(:filters)
    users.each do |user|
      user.filters.each do |filter|
        # Здесь проверяем на все фильтры чтобы потом показать юзеру по каким фильтрам конкретно оповещение
        # можно было бы проверять до первого совпадения по одному из фильтров так как событие одно и тоже и можно сэкономить на проверках просто показав юзеру что под его фильтры в принципе есть новое событие
        attrs = filter.attributes

        # приводим к формату который принимает FilterParams

        attrs['start_date'] = attrs['start_date'].strftime('%A') if attrs['start_date'].present?
        filter_params = FilterParams.new(attrs, true)
        if filter_params.success?
          params = filter_params.data

          from = params['start_from'].present?
          to = params['start_to'].present?
          at_day = params['start_date'].present?
          max_distance = params['max_distance'].present?

          @results = []

          case
            when (from && to && !at_day)
              # только по часам
              @results << check_by_hours(params, event)
            when (!from && !to && at_day)
              # только по дню
              @results << check_by_day(params, event)
            when (from && to && at_day)
              # проверяем конкретный день и по часам
              @results << check_by_day_and_hours(params, event)
            else
              nil
          end

          if @results.include? false
            next
          end

          if params['game_ids'].present?
            game_ids = params['game_ids'].reject(&:empty?)
            if game_ids.any?
              # проверка действует только если к эвенту уже сохранены игры в events_games(не успел это сделать)
              @results << event.games.pluck(:id) & game_ids.map(&:to_i)
            end
          end


          if @results.include? false
            next
          end

          # проверка по макс расстоянию
          if max_distance
            @results << check_by_distance(user, event, params['max_distance'])
          end

          # Check that all results are true
          res = @results.uniq

          if (res.length == 1) && (res[0] === true)
            if (Notification.where(user: user, event: event, filter: filter).length == 0)
              notification = user.notifications.new(event: event, filter: filter)
              notification.save
            end
          end

        end
      end
    end
  end

  def check_by_hours(params, event)
    if [params['start_from_hour_utc'], params['start_to_hour_utc']].map(&:present?).all?
      # для получения массива часов сделаем даты из часов по utc
      from_hour_with_date = DateTime.strptime(params['start_from_hour_utc'].to_s, '%H')
      to_hour_with_date = DateTime.strptime(params['start_to_hour_utc'].to_s, '%H')

      # если есть переход через сутки добавляем к второй дате 1 день для получения корректного интервала в часах для цикла
      if from_hour_with_date > to_hour_with_date
        to_hour_with_date += 1.day
      end

      hours_array = []
      (from_hour_with_date.to_i .. to_hour_with_date.to_i).step(1.hour) do |date|
        hours_array << Time.at(date).utc.hour
      end

      event.start_at.utc.hour.in? hours_array
    else
      raise 'Utc hours format is absent.'
    end
  end

  def check_by_day(params, event)
    dow1  = params['start_day_utc_range'][0].wday
    hour1 = params['start_day_utc_range'][0].hour
    dow2  = params['start_day_utc_range'][1].wday
    hour2 = params['start_day_utc_range'][1].hour

    event_start_at_utc_wday = event.start_at.utc.wday
    event_start_at_utc_hour = event.start_at.utc.hour

    result = if (dow1 == dow2)
      # сравниваем только день недели так как весь день можем в любые часы
      event_start_at_utc_wday == dow1
    else
      # сравниваем для обоих дней и промежутков часов
      ( (event_start_at_utc_wday == dow1) && (event_start_at_utc_hour >= hour1) &&  (event_start_at_utc_hour <= 23) ) || ( (event_start_at_utc_wday == dow2) && (event_start_at_utc_hour >= 0) &&  (event_start_at_utc_hour <= hour2) )
    end

    result
  end

  def check_by_day_and_hours(params, event)
    dow1  = params['start_day_utc_range'][0].wday
    hour1 = params['start_day_utc_range'][0].hour
    dow2  = params['start_day_utc_range'][1].wday
    hour2 = params['start_day_utc_range'][1].hour

    event_start_at_utc_wday = event.start_at.utc.wday
    event_start_at_utc_hour = event.start_at.utc.hour

    result = if (dow1 == dow2)
      # сравниваем день недели и часы (в этом случае день один и тот же dow1==dow2 только часы разные)
      (event_start_at_utc_wday == dow1) && (event_start_at_utc_hour >= hour1) && (event_start_at_utc_hour <= hour2)
    else
      # сравниваем для обоих дней и промежутков часов
      ( (event_start_at_utc_wday == dow1) && (event_start_at_utc_hour >= hour1) &&  (event_start_at_utc_hour <= 23) ) || ( (event_start_at_utc_wday == dow2) && (event_start_at_utc_hour >= 0) &&  (event_start_at_utc_hour <= hour2) )
    end

    result
  end

  def check_by_distance(user, event, max_distance)
    distance = user.distance_from(event)
    distance.to_i <= max_distance.to_i
  end

end