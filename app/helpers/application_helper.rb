module ApplicationHelper

  def date_format(date, format=nil)
    date.strftime(format ? format : "%d.%m.%Y %H:%M")
  end

  def current_user
    (Rails.env.development? || Rails.env.test?) ? User.first : super
  end

end
