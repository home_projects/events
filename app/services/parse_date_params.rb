class ParseDateParams
  attr_reader :data
  attr_reader :errors

  def initialize(params={})
    @success = false
    @errors = []

    if params.present?

      [:start_from, :start_to, :start_date].each do |attr|
        # сразу приводим к nil параметры которые не заданы
        params[attr.to_s] = params[attr.to_s].present? ? params[attr.to_s] : nil

        case
          when (attr == :start_from) || (attr == :start_to)
            params["#{attr.to_s}_hour_utc"] = params[attr.to_s].present? ? parse_datetime(params[attr.to_s], '%H:%M %Z').hour : nil
          when attr == :start_date
            if params[attr.to_s].present?
              start_date_obj = parse_date(params[attr.to_s], '%A')
              # нужна дата без времени для конструирования даты с конкретным временем при сравнении по дню и конкретным часам
              params['start_date_obj'] = start_date_obj
              # дата с начала дня
              beginning_of_a_day = start_date_obj.beginning_of_day
              params['start_date'] = beginning_of_a_day
              # интервал для дня переведенный в utc
              params['start_day_utc_range'] = day_utc_range beginning_of_a_day
              # интервал для дня и конкретных часов переведенный в utc (если часы тоже заданы)
              params['start_day_with_hours_utc_range'] = day_with_hours_utc_range(start_date_obj, params['start_from'], params['start_to']) if [params['start_from'], params['start_to']].map(&:present?).all?
            else
              nil
            end
          else
            params[attr.to_s] = params[attr.to_s].present? ? parse_datetime(params[attr.to_s], '%d.%m.%Y %H:%M %Z') : nil
        end
      end

      if params['start_day_with_hours_utc_range'].present?
        if params['start_day_with_hours_utc_range'][0] > params['start_day_with_hours_utc_range'][1]
          @errors << 'You should set start_from < start_to if you set specific day'
          return false
        end
      end

      if params['game_ids'].present?
        if params['game_ids'].reject(&:empty?).any?
          params['game_ids'] = params['game_ids'].reject(&:empty?)
        end
      end

      @data = params
      @success = true
    else
      @errors << 'Date params processing error.'
    end

  end

  def day_utc_range date_beginning
    utc_start = date_beginning.utc
    utc_end = utc_start + 23.hour
    [utc_start, utc_end]
  end

  def day_with_hours_utc_range(start_date_obj, start_from, start_to)
    start_from_day_with_hour_str = start_date_obj.to_s + ' ' + start_from
    start_to_day_with_hour_str = start_date_obj.to_s + ' ' + start_to

    utc_start = DateTime.strptime(start_from_day_with_hour_str, '%Y-%m-%d %H:%M %Z').utc
    utc_end = DateTime.strptime(start_to_day_with_hour_str, '%Y-%m-%d %H:%M %Z').utc
    [utc_start, utc_end]
  end

  def format_utc_datetime datetime
    datetime.utc
  end

  def parse_datetime(date, format)
    # парсим время (оно приходит с зоной клиента)
    datetime = DateTime.strptime(date, format)
    # приводим к utc
    format_utc_datetime datetime
  end

  def parse_date(date, format)
    # парсим время (оно приходит с зоной клиента)
    date = Date.strptime(date, format)
    date
  end

  def success?
    @success
  end

end