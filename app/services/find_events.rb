class FindEvents
  attr_reader :data
  attr_reader :errors

  def initialize(params, current_user)
    @success = false
    @errors = []
    @params = params
    @current_user = current_user

    get_events
  end

  def get_events
    # получаем обработанные параметры в нужных форматах либо nil
    params = FilterParams.new(@params, true)

    if params.success?
      data = params.data

      # Фильтруем события в соответствии с параметрами
      events = FilterEvents.new(data, @current_user)

      if events.success?
        @success = true
        @data = events.data
      else
        @errors += events.errors
      end

    else
        @errors << params.errors
    end
  end

  def success?
    @success
  end

end