class FilterEvents
  attr_reader :data
  attr_reader :errors

  def initialize(params, current_user)
    @params = params
    @current_user = current_user
    @errors = []
    @success = false

    filter_events
  end

  def filter_events
    from = @params['start_from'].present?
    to = @params['start_to'].present?
    at_day = @params['start_date'].present?
    max_distance = @params['max_distance'].present?

    # Запрос по времени должен присоединяться к запросу по удаленности Event.near([ 59.926119, 30.318146], max_distance) || Event.all
    events = if max_distance
       Event.near(@current_user.coordinates, @params['max_distance'])
    else
      Event.all
    end

    if @params['game_ids'].present?
      game_ids = @params['game_ids'].reject(&:empty?)
      if game_ids.any?
        events = events.joins(:games).where(games: {id: game_ids})
      end
    end

    sql_where = case
      when (from && to && at_day)
        # ищем в конкретный день и по часам
        by_day_and_hours
      when (from && to && !at_day)
        # ищем только по часам
        by_hours
      when (!from && !to && at_day)
        # ищем только по дню
        by_day
      else
        nil
    end

    if @errors.empty?
      @success = true
      @data = events.where(sql_where).where.not(user_id: @current_user.id)
    end
  end

  def by_hours
    # ! start_from & start_to точно заданы
    if [@params['start_from_hour_utc'], @params['start_to_hour_utc']].map(&:present?).all?
      # для получения массива часов сделаем даты из часов по utc
      from_hour_with_date = DateTime.strptime(@params['start_from_hour_utc'].to_s, '%H')
      to_hour_with_date = DateTime.strptime(@params['start_to_hour_utc'].to_s, '%H')

      # если есть переход через сутки добавляем к второй дате 1 день для получения корректного интервала в часах для цикла
      if from_hour_with_date > to_hour_with_date
        to_hour_with_date += 1.day
      end

      hours_array = []
      (from_hour_with_date.to_i .. to_hour_with_date.to_i).step(1.hour) do |date|
        hours_array << Time.at(date).utc.hour
      end

      sql_where = "date_part('hour', events.start_at) in (#{hours_array.join(',')})"

      sql_where
    else
      @errors << 'Cant get hours in utc'
      return false
    end

  end

  def by_day
    dow1  = @params['start_day_utc_range'][0].wday
    hour1 = @params['start_day_utc_range'][0].hour
    dow2  = @params['start_day_utc_range'][1].wday
    hour2 = @params['start_day_utc_range'][1].hour

    sql_where = if (dow1 == dow2)
      # сравниваем только день недели так как весь день можем в любые часы
      "extract(DOW FROM events.start_at) = #{dow1}"
    else
      # сравниваем для обоих дней и промежутков часов
      " ( (extract(DOW FROM events.start_at) = #{dow1}) AND (extract(HOUR FROM events.start_at) >= #{hour1}) AND (extract(HOUR FROM events.start_at) <= 23) ) OR  ( (extract(DOW FROM events.start_at) = #{dow2}) AND (extract(HOUR FROM events.start_at) >= 0) AND (extract(HOUR FROM events.start_at) <= #{hour2}) ) "
    end

    sql_where
  end

  def by_day_and_hours
    dow1  = @params['start_day_with_hours_utc_range'][0].wday
    hour1 = @params['start_day_with_hours_utc_range'][0].hour
    dow2  = @params['start_day_with_hours_utc_range'][1].wday
    hour2 = @params['start_day_with_hours_utc_range'][1].hour

    sql_where = if (dow1 == dow2)
      # сравниваем день недели и часы (в этом случае день один и тот же dow1==dow2 только часы разные)
      "(extract(DOW FROM events.start_at) = #{dow1}) AND (extract(HOUR FROM events.start_at) >= #{hour1}) AND (extract(HOUR FROM events.start_at) <= #{hour2})"
    else
      # сравниваем для обоих дней и промежутков часов
      " ( (extract(DOW FROM events.start_at) = #{dow1}) AND (extract(HOUR FROM events.start_at) >= #{hour1}) AND (extract(HOUR FROM events.start_at) <= 23) ) OR  ( (extract(DOW FROM events.start_at) = #{dow2}) AND (extract(HOUR FROM events.start_at) >= 0) AND (extract(HOUR FROM events.start_at) <= #{hour2}) ) "
    end

    sql_where
  end

  def success?
    @success
  end

end