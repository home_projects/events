FactoryGirl.define do

  factory :user do
    sequence :email do |n|
      "user#{n}@domain.com"
    end
    password '11111111'
    password_confirmation '11111111'
    # Geo
    # spb default
    # we get user coords trough user.ip method
  end

  factory :event do
    user
    start_at DateTime.parse('Mon, 05 Jun 2016 23:00:00 MSK +03:00')
    end_at   DateTime.parse('Mon, 06 Jun 2016 04:00:00 MSK +03:00')
    # Geo
    # msk default
    latitude  55.739482
    longitude 37.621307
    end

  factory :filter do
    user
    start_from '00:00 +03:00'
    start_to     '05:00 +03:00'
    start_date DateTime.parse('06 Jun 2016 00:00:00 MSK +03:00')
    max_distance 1000
    game_ids []
  end

  factory :game do
    name 'game name'
  end

end


