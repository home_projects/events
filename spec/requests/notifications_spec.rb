RSpec.describe "Notifications", type: :request do

  context 'User have filters (user_with_filters)' do
    # Users
    let!(:user_with_filters) {FactoryGirl.create(:user)}
    let!(:user_that_create_events) {FactoryGirl.create(:user)}

    let!(:filter1) {FactoryGirl.create(:filter, user: user_with_filters,
                                       # out
                                       start_from: nil,
                                       start_to: nil,
                                       start_date: DateTime.parse('09 Jun 2016 00:00:00 MSK +03:00'),
                                       max_distance: nil)}

    let!(:filter2) {FactoryGirl.create(:filter, user: user_with_filters,
                                       # out
                                       start_from: '12:00 +03:00',
                                       start_to: '15:00 +03:00',
                                       start_date: nil,
                                       max_distance: nil)}

    def create_event
      FactoryGirl.create(:event, user: user_that_create_events,
                                 # 13-е Понедельник
                                 start_at: (DateTime.parse('13 Jun 2016 02:00:00 MSK +03:00')),
                                 end_at:   DateTime.parse('06 Jun 2016 03:00:00 MSK +03:00') )
    end

    # мы сразу проверяем также что filter1 и filter2 не срабатывают на созданное событие
    # событие одно и тоже фильтры меняются

    it 'user get notification by his own filter after event has created' do
      filter = FactoryGirl.create(:filter, user: user_with_filters,
                                         start_from: '00:00 +03:00',
                                         start_to: '05:00 +03:00',
                                         # 6-е Понедельник
                                         start_date: DateTime.parse('06 Jun 2016 00:00:00 MSK +03:00'),
                                         max_distance: 1000)

      # подходит для filter по всем параметрам
      event = create_event

      notifications = Notification.all.order('id asc')
      last_notification = Notification.last
      expect(notifications.length).to eq 1
      expect(last_notification.user_id).to eq user_with_filters.id
      expect(last_notification.event_id).to eq event.id
      expect(last_notification.filter_id).to eq filter.id
    end

    it 'by filter with only hours' do
      filter = FactoryGirl.create(:filter, user: user_with_filters,
                                  start_from: '00:00 +03:00',
                                  start_to: '05:00 +03:00',
                                  start_date: nil,
                                  max_distance: nil)

      # подходит для filter по всем параметрам
      event = create_event

      notifications = Notification.all.order('id asc')
      last_notification = Notification.last
      expect(notifications.length).to eq 1
      expect(last_notification.user_id).to eq user_with_filters.id
      expect(last_notification.event_id).to eq event.id
      expect(last_notification.filter_id).to eq filter.id
    end

    it 'by filter with only date' do
      filter = FactoryGirl.create(:filter, user: user_with_filters,
                                  # 6-е Понедельник
                                  start_date: DateTime.parse('06 Jun 2016 00:00:00 MSK +03:00'),
                                  start_from: nil,
                                  start_to: nil,
                                  max_distance: nil)

      # подходит для filter по всем параметрам
      event = create_event

      notifications = Notification.all.order('id asc')
      last_notification = Notification.last
      expect(notifications.length).to eq 1
      expect(last_notification.user_id).to eq user_with_filters.id
      expect(last_notification.event_id).to eq event.id
      expect(last_notification.filter_id).to eq filter.id
    end

    it 'by filter with only distance' do
      sleep 1
      filter = FactoryGirl.create(:filter, user: user_with_filters,
                                  start_date: nil,
                                  start_from: nil,
                                  start_to: nil,
                                  max_distance: 1000)

      # подходит для filter по всем параметрам
      event = create_event

      notifications = Notification.all.order('id asc')
      last_notification = Notification.last
      expect(notifications.length).to eq 1
      expect(last_notification.user_id).to eq user_with_filters.id
      expect(last_notification.event_id).to eq event.id
      expect(last_notification.filter_id).to eq filter.id
    end

    it 'user cant get notification by filter with distance if distance to event more than max_distance in filter' do
      sleep 1
      filter_out = FactoryGirl.create(:filter, user: user_with_filters,
                                         # out
                                         max_distance: 1)

      # НЕ подходит для filter
      event = create_event

      notifications = Notification.all
      expect(notifications.length).to eq 0
    end

  end

end
