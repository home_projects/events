RSpec.describe "Filters", type: :request do

  context 'User and events' do
    # Users
    let!(:user) {FactoryGirl.create(:user)}
    let!(:user_with_events) {FactoryGirl.create(:user)}

    # Events
    let!(:event1) {FactoryGirl.create(:event, user: user_with_events,
                                      # 6-е Понедельник
                                      start_at: DateTime.parse('06 Jun 2016 00:00:00 MSK +03:00'),
                                      end_at:   DateTime.parse('06 Jun 2016 04:00:00 MSK +03:00') )
    }

    let!(:event2) {FactoryGirl.create(:event, user: user_with_events,
                                      start_at: DateTime.parse('06 Jun 2016 02:00:00 MSK +03:00'),
                                      end_at:   DateTime.parse('06 Jun 2016 04:00:00 MSK +03:00') )
    }

    let!(:event3) {FactoryGirl.create(:event, user: user_with_events,
                                      start_at: DateTime.parse('06 Jun 2016 05:00:00 MSK +03:00'),
                                      end_at:   DateTime.parse('06 Jun 2016 07:00:00 MSK +03:00') )
    }

    let!(:event4) {FactoryGirl.create(:event, user: user_with_events,
                                      # New York: Sun, 05 Jun 2016 22:00:00 EDT -04:00 | Moscow: 06 Jun 2016 05:00:00 MSK +03:00 (6-е Понедельник)
                                      start_at: DateTime.parse('06 Jun 2016 05:00:00 MSK +03:00').in_time_zone('America/New_York'),
                                      end_at:   DateTime.parse('06 Jun 2016 07:00:00 MSK +03:00') )
    }

    let!(:event5) {FactoryGirl.create(:event, user: user_with_events,
                                      # 5-е Воскресение
                                      start_at: DateTime.parse('05 Jun 2016 23:00:00 MSK +03:00'),
                                      end_at:   DateTime.parse('06 Jun 2016 01:00:00 MSK +03:00') )}

    let!(:event6) {FactoryGirl.create(:event, user: user_with_events,
                                      # 6-е Понедельник
                                      start_at: DateTime.parse('06 Jun 2016 06:00:00 MSK +03:00'),
                                      end_at:   DateTime.parse('06 Jun 2016 21:00:00 MSK +03:00') )}
    # User own Event
    let!(:event7) {FactoryGirl.create(:event, user: user,
                                      # 6-е Понедельник
                                      start_at: DateTime.parse('06 Jun 2016 02:00:00 MSK +03:00'),
                                      end_at:   DateTime.parse('06 Jun 2016 04:00:00 MSK +03:00') )
    }

    let!(:event8) {FactoryGirl.create(:event, user: user_with_events,
                                      # 13-е Понедельник
                                      start_at: (DateTime.parse('06 Jun 2016 02:00:00 MSK +03:00') + 1.week),
                                      end_at:   DateTime.parse('06 Jun 2016 03:00:00 MSK +03:00') )}

    it 'User can filter events only by hours' do

      get api_search_events_path, { format: :json, filter: {start_from: "00:00 +03:00", start_to: "05:00 +03:00", game_ids: []} }

      json = JSON.parse(response.body)

      parsed_data = JSON.parse(json['data'])
      expect(response).to be_success
      expect(parsed_data.nil?).to be false
      e_ids = parsed_data.map{|e| e['id']}
      expect(e_ids.sort).to eq [event1.id, event2.id, event3.id, event4.id, event8.id].sort
    end

    it 'User can filter events only by day of week' do

      get api_search_events_path, { format: :json, filter: {start_date: "Monday", game_ids: []} }

      json = JSON.parse(response.body)
      parsed_data = JSON.parse(json['data'])

      expect(response).to be_success
      expect(parsed_data.nil?).to be false
      e_ids = parsed_data.map{|e| e['id']}
      expect(e_ids.sort).to eq [event1.id, event2.id, event3.id, event4.id, event6.id, event8.id].sort
    end

    it 'User can filter events by day of week and hours' do

      get api_search_events_path, { format: :json, filter: {start_from: "00:00 +03:00", start_to: "05:00 +03:00", start_date: "Monday", game_ids: []} }

      json = JSON.parse(response.body)
      parsed_data = JSON.parse(json['data'])

      expect(response).to be_success
      expect(parsed_data.nil?).to be false
      e_ids = parsed_data.map{|e| e['id']}
      expect(e_ids.sort).to eq [event1.id, event2.id, event3.id, event4.id, event8.id].sort
    end

    it 'User can filter events by day of week and hours and distance' do
      event_far_away = FactoryGirl.create(:event, user: user_with_events,
                                                # 6-е Понедельник
                                                start_at: DateTime.parse('06 Jun 2016 00:00:00 MSK +03:00'),
                                                end_at:   DateTime.parse('06 Jun 2016 04:00:00 MSK +03:00'),
                                                latitude: 76.675380, longitude: 66.086683)


      get api_search_events_path, { format: :json, filter: {start_from: "00:00 +03:00", start_to: "05:00 +03:00", start_date: "Monday", max_distance: 1800, game_ids: []} }

      json = JSON.parse(response.body)
      parsed_data = JSON.parse(json['data'])

      expect(response).to be_success
      expect(parsed_data.nil?).to be false
      e_ids = parsed_data.map{|e| e['id']}

      # предыдущий результат так как событие event_far_away дальше от юзера чем 600
      expect(e_ids.sort).to eq [event1.id, event2.id, event3.id, event4.id, event8.id].sort
    end

    it 'User can filter events by day of week and hours and distance' do
      event_far_away = FactoryGirl.create(:event, user: user_with_events,
                                          # 6-е Понедельник
                                          start_at: DateTime.parse('06 Jun 2016 00:00:00 MSK +03:00'),
                                          end_at:   DateTime.parse('06 Jun 2016 04:00:00 MSK +03:00'),
                                          latitude: 76.675380, longitude: 66.086683)


      get api_search_events_path, { format: :json, filter: {start_from: "00:00 +03:00", start_to: "05:00 +03:00", start_date: "Monday", max_distance: 4000, game_ids: []} }

      json = JSON.parse(response.body)
      parsed_data = JSON.parse(json['data'])

      expect(response).to be_success
      expect(parsed_data.nil?).to be false
      e_ids = parsed_data.map{|e| e['id']}

      # добавилось событие event_far_away
      expect(e_ids.sort).to eq [event1.id, event2.id, event3.id, event4.id, event8.id, event_far_away.id].sort
    end

    it 'User can filter events only by distance' do
      event_far_away = FactoryGirl.create(:event, user: user_with_events,
                                          # 6-е Понедельник
                                          start_at: DateTime.parse('06 Jun 2016 00:00:00 MSK +03:00'),
                                          end_at:   DateTime.parse('06 Jun 2016 04:00:00 MSK +03:00'),
                                          latitude: 76.675380, longitude: 66.086683)


      get api_search_events_path, { format: :json, filter: {max_distance: 1800, game_ids: []} }

      json = JSON.parse(response.body)
      parsed_data = JSON.parse(json['data'])

      expect(response).to be_success
      expect(parsed_data.nil?).to be false
      e_ids = parsed_data.map{|e| e['id']}

      # не попадает событие event_far_away и собственное(event7), остальные - да
      expect(e_ids.sort).to eq [event1.id, event2.id, event3.id, event4.id, event5.id, event6.id, event8.id].sort
    end

    it 'User can save filter' do
      start_from = "00:00 +03:00"
      start_to = "05:00 +03:00"
      start_date = "Monday"
      max_distance = 2000
      game1 = FactoryGirl.create(:game)
      game2 = FactoryGirl.create(:game)
      game_ids = [game1.id, game2.id]

      post api_filters_path, { format: :json, filter: {start_from: start_from, start_to: start_to, start_date: start_date, max_distance: max_distance, game_ids: game_ids} }

      json = JSON.parse(response.body)
      parsed_data = JSON.parse(json['filter'])
      expect(response).to be_success
      expect(parsed_data.nil?).to be false
      expect(parsed_data['start_from']).to eq start_from
      expect(parsed_data['start_to']).to eq start_to
      expect(parsed_data['max_distance']).to eq 2000
      parsed_date = Date.parse(parsed_data['start_date'])
      expect(parsed_date).to eq Date.parse(start_date)
      expect(parsed_data['id']).to eq Filter.last.id
    end

    it 'User cant save filter without params' do
      post api_filters_path, { format: :json, filter: {start_from: '', start_to: '', start_date: '', max_distance: '', game_ids: []} }

      json = JSON.parse(response.body)
      expect(response.status).to eq 422
      expect(json['errors'].present?).to be true
    end

    it 'User cant filter events only by one hour from range' do

      get api_search_events_path, { format: :json, filter: {start_to: "05:00 +03:00", game_ids: []} }

      json = JSON.parse(response.body)
      expect(response.status).to eq 422
      expect(json['errors'].present?).to be true
    end

    it 'User cant set start hour > end hour with start day' do

      get api_search_events_path, { format: :json, filter: {start_form: "15:00 +03:00", start_to: "05:00 +03:00", start_date: 'Monday', game_ids: []} }

      json = JSON.parse(response.body)
      expect(response.status).to eq 422
      expect(json['errors'].present?).to be true
    end


    # todo errors

  end

end
