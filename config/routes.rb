Rails.application.routes.draw do

  # /
  root 'events#index'
  resources :events, :only => [:index]

  namespace :api do
    get 'search/events', to: 'search#events'
    post 'filters', to: 'filters#create'
  end

  # для наглядности
  resources  :filters, only: [:index, :create, :destroy]
  resources  :notifications, only: [:index, :show]
end
