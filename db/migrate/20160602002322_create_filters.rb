class CreateFilters < ActiveRecord::Migration
  def up
    create_table :filters do |t|
      t.string :name
      t.string :start_from
      t.string :start_to
      t.column :start_date, :datetime
      t.integer :max_distance
      t.references :user, index: true
      t.string :game_ids, array: true, default: []
      t.timestamps null: false
    end
  end

  def down
    drop_table :filters
  end
end
