class AddLatitudeAndLongitudeToEvents < ActiveRecord::Migration
  def change
    add_column :events, :latitude, :float, index: true
    add_column :events, :longitude, :float, index: true
  end
end
