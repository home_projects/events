class CreateNotifications < ActiveRecord::Migration
  def up
    create_table :notifications do |t|
      t.references :user, null: false, index: true
      t.references :event, null: false, index: true
      t.references :filter, null: false, index: true
      t.boolean :reviewed, null: false, default: false
      t.timestamps null: false
    end
  end

  def down
    drop_table :notifications
  end
end
