class CreateEventsGames < ActiveRecord::Migration
  def change
    create_table :events_games do |t|
      t.references :event, null: false, index: true
      t.references :game, null: false, index: true
      t.timestamps null: false
    end
  end
end
