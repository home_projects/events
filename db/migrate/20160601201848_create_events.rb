class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :address
      t.column :start_at, :datetime, null: false
      t.column :end_at, :datetime, null: false
      t.references :user, null: false, index: true
      t.timestamps null: false
    end
  end
end
