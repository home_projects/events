class CreateGames < ActiveRecord::Migration
  def up
    create_table :games do |t|
      t.string :name, null: false
      t.timestamps null: false
    end
  end

  def down
    drop_table :games
  end
end
