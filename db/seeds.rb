User.create(email: "user1@domain.com",password: 11111111, password_confirmation: 11111111)
User.create(email: "user2@domain.com",password: 11111111, password_confirmation: 11111111, current_sign_in_ip: '101.129.128')

coords = [
        # spb
        {latitude: 59.926119, longitude: 30.318146},
        # mos
        {latitude: 55.739482, longitude: 37.621307},
        # Тверь
        {latitude: 56.51, longitude: 35.55},

        {latitude: 37.664111, longitude: 55.722065},
        {latitude: 56.305400, longitude: 58.002400},
        {latitude: 76.675380, longitude: 66.086683}

]

last_user = User.last

30.times do |i|
  coords_index = rand(1..5)
  Event.create(name: "event_#{i}", start_at: Date.current.to_datetime + i.hour, end_at: Date.current.to_datetime + ((i + 3).hour), latitude: coords[coords_index][:latitude], longitude: coords[coords_index][:longitude], user_id: last_user.id )
end

events_all = Event.all
10.times do |i|
  game = Game.create(name: "game_#{i+1}")
  events_shuffle = events_all.shuffle
  event1 = events_shuffle.first
  event2 = events_shuffle.last

  EventsGame.create(event: event1, game: game)
  EventsGame.create(event: event2, game: game)
end

